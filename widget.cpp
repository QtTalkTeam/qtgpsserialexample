#include "widget.h"
#include "ui_widget.h"

#include <QtSerialPort>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);


    //
    // Set Style Sheet
    //
    QFile file(":/data/appCss.txt");
    file.open(QFile::ReadOnly);
    QString mStyleSheet = QLatin1String(file.readAll());
    this->setStyleSheet(mStyleSheet);


    //
    // Example use QserialPortInfo
    //
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
        qDebug() << "Name : " << info.portName();
        qDebug() << "Description : " << info.description();
        qDebug() << "Manufacturer: " << info.manufacturer();
        // Example use QSerialPort
        QSerialPort serial;
        serial.setPort(info);

        if (serial.open(QIODevice::ReadWrite))
            serial.close();
    }

    pSerial = new QSerialPort("/dev/ttymxc2",this);

    connect(pSerial, SIGNAL(readyRead()), this , SLOT(readDataSlot()));

    if(pSerial->open(QIODevice::ReadOnly)){
        qDebug() << "aperta";
    }

    qDebug() << "OK";

}

Widget::~Widget()
{
    delete ui;
}

QString Widget::dataFormat(QString tm){

    QString s;
    s.append(tm.left(2));
    s.append('-');
    s.append(tm.mid(2,2));
    s.append('-');
    s.append(tm.mid(4,2));
    return(s);
}

QString Widget::timeStampFormat(QString tm){

    QString s;
    s.append(tm.left(2));
    s.append(':');
    s.append(tm.mid(2,2));
    s.append(':');
    s.append(tm.mid(4,2));

    return(s);
}

float Widget::convert_degrees_minutes_seconds_to_decimal_Ddegree(int Degrees, float Minutes){


    float d;

    if(Degrees > 0)
        d = /*(Seconds/3600) +*/ (Minutes/60) + Degrees;
    else
        d = /*- (Seconds/3600)*/ - (Minutes/60) + Degrees;

    return(d);
}

void Widget::processNmeaString(QString nmea){

    nmea = nmea.trimmed().simplified();

    if(nmea.at(0) == '$'){

        if(nmea.startsWith("$PSTM")){

            //qDebug() << "$PSTM";

        }else
        if(nmea.startsWith("$GPGSV")){

            //qDebug() << "$GPGSV";

        }else
        if(nmea.startsWith("$GPRMC")){

            //qDebug() << "$GPRMC";
            qDebug() << nmea;

            QStringList sl = nmea.split(',');

            //$GPRMC,<Timestamp>,<Status>,<Lat>,<N/S>,<Long>,<E/W>,<Speed>,
            //<Course>,<Date>,<MagVar>,<MagVarDir>,<Mode>*<checksum><cr><lf>

            //qDebug() << "Timestamp :" << sl.at(1);
            ui->timeStampLabel->setText(timeStampFormat(sl.at(1)));

            //qDebug() << "Status    :" << sl.at(2);
            if(sl.at(2) == 'A'){
                ui->statusLabel->setText("Data valid");
            }else{
                ui->statusLabel->setText("Navigation warning");
            }

            //qDebug() << "Lat       :" << sl.at(3);
            //qDebug() << "N/S       :" << sl.at(4);
            ui->latitudineLabel->setText(QString("%1° %2").arg(sl.at(3)).arg(sl.at(4)));

            //qDebug() << "Long      :" << sl.at(5);
            //qDebug() << "E/W       :" << sl.at(6);
            ui->longitudineLabel->setText(QString("%1° %2").arg(sl.at(5)).arg(sl.at(6)));

            //qDebug() << "Speed     :" << sl.at(7);
            ui->speedLabel->setText(QString("%1 m/s").arg(sl.at(7)));

            //qDebug() << "Course    :" << sl.at(8);

            //qDebug() << "Date      :" << sl.at(9);
            ui->dateLabel->setText(dataFormat(sl.at(9)));
            //qDebug() << "MagVar    :" << sl.at(10);
            //qDebug() << "MagVarDir :" << sl.at(11);
            //qDebug() << "Mode      :" << sl.at(12);

        }else
        if(nmea.startsWith("$GPGGA")){

            //qDebug() << "$GPGGA";


            QStringList sl = nmea.split(',');

            //$GPGGA,<Timestamp>,<Lat>,<N/S>,<Long>,<E/W>,<GPSQual>,<Sats>,
            //<HDOP>,<Alt>,M,<GEOSep>,M,<DGPSAge>,<DGPSRef>
            //*<checksum><cr><lf>

            //qDebug() << "Timestamp :" << sl.at(1);
            ui->timeStampLabel->setText(timeStampFormat(sl.at(1)));

            //GPS Format: DDMM.MMMM Latitude (DegreesMinutes.FractionalMinute)
            //qDebug() << "Lat       :" << sl.at(2);
            //qDebug() << "N/S       :" << sl.at(3);
            int     DD = QString(sl.at(2)).mid(0,2).toInt();
            float   MM = QString(sl.at(2)).mid(3).toFloat();

            float decimaDegree = convert_degrees_minutes_seconds_to_decimal_Ddegree(DD,MM);

            ui->latitudineLabel->setText(QString("%1°%2' %3 -  %4").arg(DD).arg(MM).arg(sl.at(3)).arg(decimaDegree));

            //GPS Format: DDDMM.MMMM Longitude (DegreesMinutes.FractionalMinute)
            //qDebug() << "Long      :" << sl.at(4);
            //qDebug() << "E/W       :" << sl.at(5);

            int     DDD = QString(sl.at(4)).mid(0,3).toInt();
            float   MMM = QString(sl.at(4)).mid(4).toFloat();

            decimaDegree = convert_degrees_minutes_seconds_to_decimal_Ddegree(DDD,MMM);

            ui->longitudineLabel->setText(QString("%1°%2' %3 -  %4").arg(sl.at(DDD)).arg(MMM).arg(sl.at(5)).arg(decimaDegree));

            //qDebug() << "GPSQual     :" << sl.at(6);
            if(sl.at(6) == '0')
                ui->gPSQualLabel->setText("Invalid");
            else
                if(sl.at(6) == '1')
                    ui->gPSQualLabel->setText("Valid GNSS fix");
            else
                if(sl.at(6) == '2')
                    ui->gPSQualLabel->setText("Valid differential GNSS fix");

            //qDebug() << "Num.Satellite     :" << sl.at(7);
            ui->numSatLabel->setText(sl.at(7));

            //OTher

        }else
        if(nmea.startsWith("$--GSV")){

            //qDebug() << "$--GSV";

        }else
        if(nmea.startsWith("$GNGSA")){

            //qDebug() << "$GNGSA";

            //$GNGSA,<Mode1>,<Mode2>,[<SatPRN1>],...,[<SatPRNn>],<PDOP>,
            //<HDOP>,<VDOP>*<checksum><cr><lf>

        }else
        if(nmea.startsWith("$GPGGA")){

            //qDebug() << "$GPGGA";

        }else
        if(nmea.startsWith("$GLGSV")){

            //qDebug() << "$GLGSV";

        }else

            qDebug() << "!! " << nmea << '\n';

    }
}

void Widget::readDataSlot(){

    QByteArray data = pSerial->readAll();
    QString s = QString(data.toStdString().data());

    foreach (QChar c, s) {

        if((c != '\n')and(c != '\r')){

            m_NmeaString.append(c);

        }else{

            if(m_NmeaString.length()){

                ui->logNmea->append( m_NmeaString );
                processNmeaString( m_NmeaString );

                m_NmeaString = QString::null;
            }
        }
    }
}

void Widget::on_gpsLogPushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void Widget::on_clearLogPushButton_clicked()
{
    ui->logNmea->clear();
}

void Widget::on_closeLogPushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}
