#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>

class QSerialPort;
class QNmeaPositionInfoSource;


namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;

    QString m_NmeaString;

    QSerialPort* pSerial;
    void processNmeaString(QString nmea);
    QString timeStampFormat(QString tm);
    QString dataFormat(QString tm);

    float convert_degrees_minutes_seconds_to_decimal_Ddegree(int Degrees, float Minutes);

private slots:
    void readDataSlot();

    void on_gpsLogPushButton_clicked();
    void on_clearLogPushButton_clicked();
    void on_closeLogPushButton_clicked();
};

#endif // WIDGET_H
